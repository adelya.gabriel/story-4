from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('education/', views.education, name='education'),
    path('experience/', views.experience, name='experience'),
    path('skill/', views.skill, name='skill'),
    path('songs/', views.songs, name='songs'),
]