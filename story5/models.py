from django.db import models

class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=250)
    dosen = models.CharField(max_length=500)
    jumlah_sks = models.PositiveIntegerField(default=3)
    deskripsi = models.CharField(max_length=1000)
    tahun_semester = models.CharField(max_length=250)
    ruang_kelas = models.CharField(max_length=300)

    def __str__(self):
        return self.nama_matkul

